# elixi.re Admin API Documentation

The Admin API is a subset of elixi.re's API. Because of that,
it follows the same error codes, authentication and docs from the API.

## Types

### `raw_user`

```js
{
  user_id: string,
  username: string,
  active: boolean,
  admin: boolean,
  domain: number,
  subdomain: ?string,
  email: string,
  paranoid: boolean,
  consented: boolean,
}
```

### `search_result`

```js
{
  user_id: string,
  username: string,
  active: boolean,
  admin: boolean,
  consented: boolean
}
```

### `limits`

```js
{
  limit: number,
  used: number,
  shorten_limit: number,
  shorten_used: number
}
```

## Basic

### `GET /api/admin/test`

- Authentication required

Test route. Check if you're an admin or not.
Raises an error if you aren't.

#### Output

```js
{
  admin: boolean
}
```


## Users

### `GET /api/admin/users/<user_id:int>`

- Requires Admin Authentication

Get information about a single user.

#### Output

`raw_user` with an extra `limits` field (`limits` type).

```js
raw_user
```

### `GET /api/admin/listusers/<page:int>`

- Requires Admin Authentication.

A list of users in the instance.

#### Output

```js
raw_user[]
```

### `GET /api/admin/list_inactive/<page:int>`

- Requires Admin Authentication.

List *only inactive* users in the instance.

#### Output

```js
raw_user[]
```

### `POST /api/admin/search/user/<page:int>`

- Requires Admin Authentication.

Search for users in the instance.

#### Input

```js
{
  search_term: string
}
```

#### Output

```js
raw_user[]
```

### `GET /api/admin/users/search`

- Requires Admin Authentication

#### Input

Inputs to this route are given using query arguments
instead of a JSON body.

This object is not describing a JSON body.

```js
{
  // if you want to search active or inactive users
  active: "true" | "false",

  // the string you want to search users with, can be:
  //  - part of a username
  //  - part of a user id
  query: string,

  // Current page you want (starts from 0)
  page: number,

  // How many users per page?
  per_page: number
}
```

#### Output

```js
{
  results: search_result[],
  // pagination info
  pagination: {
    // The total number of pages available
    // given the query.
    total: number,

    // The current page
    current: number
  }
}
```

### `POST /api/admin/activate/<user_id:int>`

- Requires Admin Authentication.

Activate a user's account. This will send an email to them
notifying of the activation.

#### Output

```js
{
    success: boolean,
    result: string
}
```

### `POST /api/admin/deactivate/<user_id:int>`

- Requires Admin Authentication.

Deactivate a user's account.

#### Output

```js
{
    success: boolean,
    result: string
}
```

### `POST /api/admin/activate_email/<user_id:int>`

- Requires Admin Authentication.

Send an email to the user so they activate their
account manually. This is the preferred method
to activate accounts, as to get the account activated,
you need an existing email to start with.

#### Output

```js
{
    success: boolean
}
```

### `PATCH /api/admin/user/<user_id:int>`

- Requires Admin Authentication.

Modify a single user.

#### Input

```js
{
  ?admin: ?boolean,

  // use bytes
  ?upload_limit: ?number,

  ?shorten_limit: ?number
}
```

#### Output

```js
patch_user_fields: "admin" | "upload_limit" | "shorten_limit"

patch_user_fields[]
```

### `DELETE /api/admin/user/<user_id:int>`

- Requires Admin Authentication

Delete a single user from the instance.

#### Output

```js
{
  success: boolean
}
```

## Files and Shortens

### `GET /api/admin/file/<shortname>` 

- Requires Admin Authentication.

Fetch information about a file.

#### Output

```js
{
  file_id: string,
  mimetype: string,
  filename: string,
  file_size: number,
  uploader: string,
  fspath: string,
  deleted: boolean,
  domain: number
}
```

### `GET /api/admin/shorten/<shortname>`

- Requires Admin Authentication.

Fetch information about a shorten.

#### Output

```js
{
  shorten_id: string,
  filename: string,
  redirto: string,
  uploader: string,
  deleted: boolean,
  domain: number
}
```


### `PATCH /api/admin/file/<file_id:int>`

- Requires Admin Authentication.

Modify a file.

#### Input

```js
{
  ?domain_id: number,
  ?shortname: string,
}
```

#### Output

A list of fields that were updated

```js
patch_file_field = "domain" | "shortname"

patch_file_field[]
```


### `PATCH /api/admin/shorten/<shorten_id:int>`

- Requires Admin Authentication.

Modify a shorten.

#### Input

```js
{
  ?domain_id: number,
  ?shortname: string,
}
```

#### Output

A list of fields that were updated

```js
patch_file_field = "domain" | "shortname"

patch_file_field[]
```



### `DELETE /api/admin/file/<file_id:int>`

- Requires Admin Authentication.

Delete a single file.

#### Output

```js
{
  shortname: string,
  uploader: string,
  success: boolean
}
```

### `DELETE /api/admin/shorten/<shorten_id:int>`

- Requires Admin Authentication.

Delete a single shorten.

#### Output

```js
{
  shortname: string,
  uploader: string,
  success: boolean
}
```

## Domains


### `GET /api/admin/domains/<domain_id:int>`

- Requires Admin Authentication.

Get statistics about a domain.

#### Output

```js
domain_info:

{
  info: {
    domain: string,
    official: boolean,
    admin_only: boolean,
    cf_enabled: boolean,
    owner: {
      user_id: string,
      username: string,
      consented: boolean,
      admin: boolean,
    }
  },

  // Actual statistics about the domain
  stats: {
    users: number,
    files: number,
    shortens: number
  },

  // Statistics about users that complied with our
  // data policy
  public_stats: {
    users: number,
    files: number,
    shortens: number
  }
}
```


### `GET /api/admin/domains`

- Requires Admin Authentication

Get statistics about all domains in the instance.

#### Output

```js
{
  [number]: domain_info
}
```


### `PUT /api/admin/domains`

- Requires Admin Authentication.

Add a new domain on the instance.

#### Input

```js
{
  domain: string,
  admin_only: boolean,
  official: boolean,

  // default permissions for the domain
  // default value for that field is 3
  permissions: number,

  // the user id representing the owner of that domain
  owner_id: string,
}
```

#### Output

```js
{
  success: boolean,
  result: string,
  new_id: number,
}
```

### `PATCH /api/admin/domains/<domain_id:int>`

- Requires Admin Authentication

Update a domain's information.

#### Input

All fields here are optional.
```js
{
  owner_id: string,
  admin_only: boolean,
  official: boolean,
  cf_enabled: boolean,
  permissions: number,
}
```

#### Output

```js
{
  updated: array,
}
```


### `PUT /api/admin/domains/<domain_id:str>/owner`

Add an owner to a domain.

#### Input

```js
{
  owner_id: string
}
```

#### Output

```js
{
  success: boolean,
  output: string,
}
```

### `POST /api/admin/email_domain/<domain_id:int>`

- Requires Admin Authentication

Send an email to the domain owner.

#### Input

```js
{
  subject: string,
  body: string,
}
```

#### Output

```js
{
  success: boolean,
  owner_id: string,
  owner_email: string,
}
```

### `DELETE /api/admin/domains/<domain_id:int>`

- Requires Admin Authentication.

Delete a domain from the instance.
This:
 - Moves all files to domain 0.
 - Moves all shortens to domain 0.
 - Moves all users to domain 0.

#### Output

```js
{
  success: boolean,
  file_move_result: string,
  shorten_move_result: string,
  users_move_result: string,
  users_shorten_move_result: string,
  result: string
}
```

## Misc

### `POST /api/admin/broadcast`

- Requires Admin Authentication

Broadcast an email to all users of the instance.

#### Input
```js
{
  subject: string,
  body: string,
}
```

#### Output

```js
{
  success: boolean,
}
```
